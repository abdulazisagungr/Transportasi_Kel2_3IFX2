package com.abdul.transportasi;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class TransRide extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trans_ride);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        TextView jna = (TextView) findViewById(R.id.jna);
        Bundle bun = getIntent().getExtras();
        jna.setText(bun.getString("jenis"));


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-6.949755, 107.624578);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    public void driverRide(View view){
        TextView jna = (TextView) findViewById(R.id.jna);
        String b = jna.getText().toString();
        if (b.equals("1")) {
            Intent a = new Intent(TransRide.this, DriverRide.class);
            startActivity(a);
        }else if (b.equals("2")){
            Intent a = new Intent(TransRide.this, DriverCar.class);
            startActivity(a);
        }else if (b.equals("3")){
            Intent a = new Intent(TransRide.this, DriverShip.class);
            startActivity(a);
        }

        else if (b.equals("4")){
            Intent a = new Intent(TransRide.this, DriverHeli.class);
            startActivity(a);
        }
    }
}
