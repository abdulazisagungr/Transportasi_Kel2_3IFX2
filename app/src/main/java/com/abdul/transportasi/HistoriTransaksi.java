package com.abdul.transportasi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class HistoriTransaksi extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_histori_transaksi);
    }

    public void pembayaran(View view){
        Intent a = new Intent(HistoriTransaksi.this,DetailHistory.class);
        startActivity(a);
    }
}
