package com.abdul.transportasi;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class DetailHistory extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_history);
    }

    public void feedback(View view){
        finish();
    }
}
