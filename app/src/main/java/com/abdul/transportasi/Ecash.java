package com.abdul.transportasi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Ecash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecash);
    }

    public void tambah(View view)
    {
        Intent a = new Intent(Ecash.this,TambahEcash.class);
        startActivity(a);
    }

    public void ambil(View view)
    {
        Intent a = new Intent(Ecash.this,ambilCash.class);
        startActivity(a);
    }
}
