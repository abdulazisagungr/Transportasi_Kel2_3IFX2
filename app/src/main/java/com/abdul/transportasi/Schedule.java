package com.abdul.transportasi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class Schedule extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
    }

    public void order(View view){
        Intent a = new Intent(Schedule.this,Menu_Utama.class);
        Toast.makeText(getApplicationContext(),"Schedule Aktif",Toast.LENGTH_SHORT).show();
        startActivity(a);
    }
}
