package com.abdul.transportasi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class TambahEcash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_ecash);
    }
    public void simpan(View view)
    {
        Toast.makeText(getApplicationContext(),"Silahkan transfer ke No Rek Trans-OL",Toast.LENGTH_SHORT).show();
        finish();
    }
}
