package com.abdul.transportasi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Rent extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rent);
    }

    public void cari(View view)
    {
        Intent a = new Intent(Rent.this,DaftarMobil.class);
        Bundle bun = new Bundle();
        bun.putString("jenis","4");
        a.putExtras(bun);

        startActivity(a);
    }
}
