package com.abdul.transportasi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class DriverShip extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_ship);
    }

    public void pembayaran(View view){
        Intent a = new Intent(DriverShip.this,PilihPembayaran.class);
        startActivity(a);
    }
}
