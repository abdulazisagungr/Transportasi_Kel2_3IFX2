package com.abdul.transportasi;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class Menu_Utama extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu__utama);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu__utama, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            Intent a = new Intent(Menu_Utama.this,HistoriTransaksi.class);
            startActivity(a);
        } else if (id == R.id.nav_gallery) {
            Intent a = new Intent(Menu_Utama.this,Ecash.class);
            startActivity(a);

        } else if (id == R.id.nav_share) {
            Intent a = new Intent(Menu_Utama.this,Profil.class);
            startActivity(a);

        } else if (id == R.id.nav_send) {

            Intent a = new Intent(Menu_Utama.this,MainActivity.class);
            startActivity(a);
            finish();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void menuRide(View view)
    {
        Intent a = new Intent(Menu_Utama.this,TransRide.class);
        Bundle bun = new Bundle();
        bun.putString("jenis","1");
        a.putExtras(bun);

        startActivity(a);
    }

    public void menuCar(View view)
    {
        Intent a = new Intent(Menu_Utama.this,TransRide.class);
        Bundle bun = new Bundle();
        bun.putString("jenis","2");
        a.putExtras(bun);

        startActivity(a);
    }

    public void menuShip(View view)
    {
        Intent a = new Intent(Menu_Utama.this,TransRide.class);
        Bundle bun = new Bundle();
        bun.putString("jenis","3");
        a.putExtras(bun);

        startActivity(a);
    }

    public void menuHeli(View view)
    {
        Intent a = new Intent(Menu_Utama.this,TransRide.class);
        Bundle bun = new Bundle();
        bun.putString("jenis","4");
        a.putExtras(bun);

        startActivity(a);
    }

    public void menuSce(View view)
    {
        Intent a = new Intent(Menu_Utama.this,Schedule.class);


        startActivity(a);
    }

    public void menuRent(View view)
    {
        Intent a = new Intent(Menu_Utama.this,Rent.class);


        startActivity(a);
    }

    public void menuBox(View view)
    {
        Intent a = new Intent(Menu_Utama.this,TransBox.class);


        startActivity(a);
    }

    public void menuSend(View view)
    {
        Intent a = new Intent(Menu_Utama.this,TransSend.class);


        startActivity(a);
    }

    public void menuFood(View view)
    {
        Intent a = new Intent(Menu_Utama.this,TransFood.class);


        startActivity(a);
    }

    public void menuTick(View view)
    {
        Intent a = new Intent(Menu_Utama.this,TransTiket.class);


        startActivity(a);
    }

    public void menuMed(View view)
    {
        Intent a = new Intent(Menu_Utama.this,TransMedis.class);


        startActivity(a);
    }


}
