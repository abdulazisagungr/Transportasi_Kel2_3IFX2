package com.abdul.transportasi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class DaftarBox extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_box);
    }

    public void pembayaran(View view){
        Intent a = new Intent(DaftarBox.this,PilihPembayaran.class);
        startActivity(a);
    }
}
