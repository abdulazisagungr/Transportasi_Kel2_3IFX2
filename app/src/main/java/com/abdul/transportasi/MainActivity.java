package com.abdul.transportasi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
    }

    public void submt(View view)
    {
        EditText tlp = (EditText) findViewById(R.id.nohp);
        EditText pass = (EditText) findViewById(R.id.pass);
        if (tlp.getText().toString().equals("user")) {

            Intent a = new Intent(MainActivity.this, Menu_Utama.class);
            startActivity(a);
            finish();
        }else{
            Intent a = new Intent(MainActivity.this, MenuDriver.class);
            startActivity(a);
            finish();
        }
    }
}
