package com.abdul.transportasi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class Tujuan extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tujuan);
    }

    public void order(View view){
        Intent a = new Intent(Tujuan.this,Menu_Utama.class);
        Toast.makeText(getApplicationContext(),"Tunggu Respon Dari Pemilik Kendaraan",Toast.LENGTH_SHORT).show();
        startActivity(a);
    }
}
