package com.abdul.transportasi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class EditProfil extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profil);
    }

    public  void  simpan(View view){
        Toast.makeText(getApplicationContext(),"Profil telah dirubah",Toast.LENGTH_SHORT).show();
        finish();
    }
}
