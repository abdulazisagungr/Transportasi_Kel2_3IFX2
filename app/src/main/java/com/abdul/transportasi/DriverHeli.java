package com.abdul.transportasi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class DriverHeli extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_heli);
    }

    public void pembayaran(View view){
        Intent a = new Intent(DriverHeli.this,PilihPembayaran.class);
        startActivity(a);
    }
}
